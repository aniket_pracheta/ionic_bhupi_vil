import {Injectable} from '@angular/core';
import {Platform} from 'ionic-angular';
import {
    Http, Headers, RequestOptions, RequestOptionsArgs, RequestMethod, Request,
    URLSearchParams
} from '@angular/http';
import 'rxjs/add/operator/map';
import {Network} from '@ionic-native/network';

import {SQLite, SQLiteObject} from '@ionic-native/sqlite';
import {Storage} from '@ionic/storage';

/* Import Loader */
import {LoadingController} from 'ionic-angular';

/* Import Crypto js for header authentication */
import * as CryptoJS from 'crypto-js';

import { LogProvider } from '../log/log';
import { GlobalVarsProvider } from '../global-vars/global-vars';

/*
 Generated class for the HttpClientProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular DI.
 */
@Injectable()
export class HttpClientProvider {
    public database: SQLite;
    customerEndPointBaseUrl: string;
    wpBaseUrl: string;
    loader: any;

    constructor(public network: Network, private storage: Storage, public http: Http, public loadingCtrl: LoadingController, private platform: Platform, private sqlite: SQLite, public logProvider: LogProvider, public globalVarsProvider: GlobalVarsProvider) {
        this.logProvider.log('HttpClientProvider');

        /*
         * Assiging the global values for Global Provider
         */
        this.customerEndPointBaseUrl = this.globalVarsProvider.getCustomerBaseUrl();
        this.wpBaseUrl = this.globalVarsProvider.getBaseUrl();

        /*
         * Description : Create a table at the time of platform ready
         */
        this.platform.ready().then(() => {

            this.logProvider.log("platform ready");
            // watch network for a disconnect
            this.network.onDisconnect().subscribe(() => {
                this.logProvider.log('network was disconnected :-(');
            });
            // watch network for a connect
            this.network.onConnect().subscribe(() => {
                this.logProvider.log('network connected!');
            });

            this.sqlite.create({
                name: 'ville.db',
                location: 'default'
            }).then((db: SQLiteObject) => {
                db.executeSql("CREATE TABLE IF NOT EXISTS theVille (id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT, response text)", []).then(res => {
                    this.logProvider.log("Table created successful.");
                }).catch(e => {
                    this.logProvider.logO("something went wrong 1 ", e);
                });
            }).catch(e => {
                this.logProvider.logO("something went wrong 2", e);
            });
        });

    }

    /*
     * Name : commonLoader
     * Description : commonLoader for whole the application
     */
    commonLoader() {
        this.loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        this.loader.present();
    }

    /**
     * Name : insertData
     * Description : Insert Api response in created table
     * @param apiResponse
     */
    insertData(apiResponse) {
        let insertQuery = "INSERT INTO theVille (key, response) VALUES (?,?)";
        return this.sqlite.create({
            name: 'ville.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
            return db.executeSql(insertQuery, [apiResponse.key, apiResponse.response]).then(res => {
                return Promise.resolve(res);
            }).catch(e => {
                return Promise.resolve(e);
            });
        }).catch(e => {
            return Promise.resolve(e);
        });
    }


    /*
     * Name : updatedata
     * Description : Update specific API response by using key
     */
    updatedata(apiResponse) {
        let updateQuery = "UPDATE theVille SET response = ? WHERE key = ?";
        return this.sqlite.create({
            name: 'ville.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
            return db.executeSql(updateQuery, [apiResponse.response, apiResponse.key]).then(res => {
                return Promise.resolve(res);
            }).catch(e => {
                return Promise.resolve(e);
            });
        }).catch(e => {
            return Promise.resolve(e);
        });
    }

    /**
     * Name : getData
     * Description : Fetch specific API Response for offline mode
     * @param key
     */
    getData(key) {
        let getQuery = "SELECT * FROM theVille WHERE key = ?";
        return this.sqlite.create({
            name: 'ville.db',
            location: 'default'
        }).then((db: SQLiteObject) => {
            return db.executeSql(getQuery, [key]).then(res => {
                let response = [];
                for (var i = 0; i < res.rows.length; i++) {
                    response.push(res.rows.item(i));
                }
                return Promise.resolve(response);
            }).catch(e => {
                return Promise.resolve(e);
            });
        }).catch(e => {
            return Promise.resolve(e);
        });
    }

    /**
     * Name : storeApiData
     * Description : Store API response for Offline mode
     * @param data
     */
    storeApiData(data) {
        this.storage.set(data.key, data.response).then((val) => {
            // this.logProvider.log('Api response=====', JSON.stringify(val));
            // this.fetchAllKey();
        });
    }

    /*
     * Name : fetchAllKey
     * Description : Fetch all available api keys
     */
    fetchAllKey() {
        this.storage.keys().then((val) => {
            this.logProvider.logO('Api response all key', JSON.stringify(val));
            this.fetchApiData(this.customerEndPointBaseUrl+"app-home-carousel");
        }).catch(e => {
            this.logProvider.logO('Api response Error = = = = ', e);
        });
    }

    /**
     * Name : fetchApiData
     * Description : Fetch specific api response for offline mode
     * @param key
     */
    fetchApiData(key) {
        return this.storage.get(key).then((val) => {
            this.logProvider.logO('Api response', JSON.stringify(val));
            return Promise.resolve(val);
        }).catch(e => {
            this.logProvider.logO('Api response Error', e);
        });
    }


    /*
     * Home page api
     */
    homePageApi() {
        return this.getVilleData(this.customerEndPointBaseUrl + 'app-home', this.encryptData(encodeURIComponent(this.customerEndPointBaseUrl + 'app-home')));
    }

    /*
     * Contact api
     */
    contactDetail() {
        return this.getVilleData(this.customerEndPointBaseUrl + 'app-contact', this.encryptData(encodeURIComponent(this.customerEndPointBaseUrl + 'app-contact')));
    }

    /*
     * subCategory block page api
     */
    subCategoryPage(data, method="GET" , commonLoader=true) {
        if(method == 'GET'){
            return this.getVilleData(this.customerEndPointBaseUrl + data.pageName, this.encryptData(encodeURIComponent(this.customerEndPointBaseUrl + data.pageName)));
        } else {

            return this.postVilleData(this.customerEndPointBaseUrl + data.pageName, this.encryptData(encodeURIComponent(this.customerEndPointBaseUrl + data.pageName)), data.dataSet, commonLoader);
        }
    }

    /*
     * subCategory detail page api
     */
    subDetailPage(data) {
        //return this.getVilleData(this.customerEndPointBaseUrl + 'app-sub-page/?slug='+data.pageName, this.encryptData(encodeURIComponent(this.customerEndPointBaseUrl + 'app-sub-page')));
            return this.getVilleData(this.customerEndPointBaseUrl + 'app-sub-page?slug=' + data.pageName, this.encryptData(encodeURIComponent(this.customerEndPointBaseUrl + 'app-sub-page?slug=' + data.pageName)));

    }


    /**
     * Name : encryptData
     * Description : encrypt value for header authentication by using CryptoJS 'HmacSHA1' function
     * @param value
     */
    encryptData(value) {
        this.logProvider.log(value);
        return CryptoJS.HmacSHA1(value, 'casinoresort');
    }


    /**
     * Name : getVilleData
     * Description : call HTTP client for fetching data
     * @param url
     * @param message
     */
    getVilleData(url, message) {
        this.commonLoader();
        this.logProvider.logO('offfline test->', this.network.type);
        if (this.network.type == "none") {
            return new Promise((resolve, reject) => {
                this.fetchApiData(url).then((data) => {
                    resolve(data);
                    this.loader.dismiss();
                });
            });
        } else {
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            headers.append('Authorization', 'Basic ' + btoa("theville:casinoresort"));
            headers.append('x-api-key', message);

            return new Promise((resolve, reject) => {
                this.http.get(url, {headers: headers})
                    .map(res => res.json())
                    .subscribe(data => {
                        resolve(data);
                        this.storeApiData({
                            "key": url,
                            "response": data
                        });

                        //this.logProvider.logO("get ville data = ", JSON.stringify(data));

                        if(this.loader){
                            this.loader.dismiss();
                            this.loader = null;
                        }
                    }, error => {
                        //this.logProvider.logO("get ville data error = ", JSON.stringify(error));

                        reject(error);
                        if(this.loader){
                            this.loader.dismiss();
                            this.loader = null;
                        }
                    });
            });
        }
    }

    postVilleData(url, message, params, commonLoader){
        if(commonLoader){
            this.commonLoader();
        } else {
            console.log('POST-> '+ this.loader);
            if(this.loader){
                this.loader.dismiss();
                this.loader = null;
            }
        }
        if (this.network.type == "none") {
            return new Promise((resolve, reject) => {
                this.fetchApiData(url).then((data) => {
                    resolve(data);
                    this.loader.dismiss();
                });
            });
        } else {
            var headers = new Headers();
            let urlSearchParams = new URLSearchParams();
            headers.append('Authorization', 'Basic ' + btoa("theville:casinoresort"));
            headers.append('x-api-key', message);
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            let body = params;
            return new Promise((resolve, reject) => {
                this.http.post(url, body,{headers: headers})
                    .map(res => res.json())
                    .subscribe(data => {
                        resolve(data);
                        this.storeApiData({
                            "key": url,
                            "response": data
                        });

                        // this.logProvider.logO("get ville data = ", JSON.stringify(data));
                        if(this.loader){
                            this.loader.dismiss();
                        }
                    }, error => {
                        // this.logProvider.logO("get ville data error = ", JSON.stringify(error));

                        reject(error);
                        if(this.loader){
                            this.loader.dismiss();
                        }
                    });
            });
        }
    }
}
