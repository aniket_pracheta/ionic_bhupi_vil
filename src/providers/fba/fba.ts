import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

import {FirebaseAnalytics} from "@ionic-native/firebase-analytics";
import {GlobalVarsProvider} from '../global-vars/global-vars';
import {LogProvider} from '../log/log';
/*
 Generated class for the FbaProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular DI.
 */
@Injectable()
export class FbaProvider {

    constructor(public globalVarsProvider: GlobalVarsProvider, public logProvider: LogProvider, public firebaseAnalytics: FirebaseAnalytics) {

    }

    postEvent(event, page) {
        if (this.globalVarsProvider.isDevice) {
            this.firebaseAnalytics.logEvent(event, {page: page})
                .then((res: any) => this.logProvider.logO("FirebaseAnalytics: ", res))
                .catch((error: any) => this.logProvider.logO("FirebaseAnalytics: ", error));
        }
    }

}
