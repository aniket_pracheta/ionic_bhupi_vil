import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Platform} from 'ionic-angular';
import 'rxjs/add/operator/map';

/*
 Generated class for the GlobalVarsProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular DI.
 */
@Injectable()
export class GlobalVarsProvider {
    private baseUrl: string;
    private customerBaseUrl: string;
    private isLive: boolean;
    public isDevice: boolean;

    constructor(protected platform: Platform) {
        var flag = false;
        this.setIsLive(flag);
        this.setBaseUrl(flag ? "https://www.the-ville.com.au/wp-json/wp/v2/" : "http://dev.the-ville.com.au/wp-json/wp/v2/");
        this.setCustomerBaseUrl(flag ? "https://www.the-ville.com.au/wp-json/custom-endpoint/v2/" : "http://dev.the-ville.com.au/wp-json/custom-endpoint/v2/");
        if (this.platform.is('core') || this.platform.is('mobileweb')) {
            this.isDevice = false;
        } else {
            this.isDevice = true;
        }
    }

    setIsLive(value) {
        this.isLive = value;
    }

    getIsLive() {
        return this.isLive;
    }

    setBaseUrl(value) {
        this.baseUrl = value;
    }

    getBaseUrl() {
        return this.baseUrl;
    }

    setCustomerBaseUrl(value) {
        this.customerBaseUrl = value;
    }

    getCustomerBaseUrl() {
        return this.customerBaseUrl;
    }

}
