import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

import {GlobalVarsProvider} from '../global-vars/global-vars';

/*
 Generated class for the LogProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular DI.
 */
@Injectable()
export class LogProvider {

    constructor(public globalVars: GlobalVarsProvider) {

    }

    log (data) {
        if (!this.globalVars.getIsLive()) {
            console.log(data);
        }
    }

    logO (data, options) {
        if (!this.globalVars.getIsLive()) {
            console.log(data, options);
        }
    }
}
