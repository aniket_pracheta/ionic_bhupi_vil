import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";

import {NotificationPage} from '../../pages/notification/notification';
import {LoginPage} from '../../pages/login/login';
import {HomePage} from '../../pages/home/home';

import {PushNotification, pushNotificationCount} from "../../global/components/push-notification";

/**
 * Generated class for the CustomHeaderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */


export interface pushNotificationCount{
    count:any,
    apiStatus: any;
}

@Component({
    selector: 'custom-header',
    templateUrl: 'custom-header.html'
})
export class CustomHeaderComponent {

    text: string;
    isLoggedinUser: any;
    info:pushNotificationCount;
    constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, public notification:PushNotification) {
        this.info = this.notification.info;
        this.notification.subscribeAnEvent('user:LoggedIn').subscribe('user:LoggedIn',(obj) => {
            this.isLoggedinUser = obj['status'];
        });
        this.storage.get('member_info').then((val) => {
            if(JSON.stringify(val).length > 0){
                this.isLoggedinUser = val['status'];
            } else {
                this.isLoggedinUser = 401;
            }

        }).catch((error) => {
            this.isLoggedinUser = 401;
            console.log("Catch Error login = ", JSON.stringify(error));
        });

        this.notification.subscribeAnEvent('user:logOut').subscribe('user:logOut', (obj) => {
            this.isLoggedinUser = obj;
        })
        console.log("login->>"+this.isLoggedinUser);
    }

    openNotification() {
        this.navCtrl.push(NotificationPage);
    }

    openHomePage() {
        this.navCtrl.popToRoot();
    }

    openLogin() {
        this.navCtrl.push(LoginPage);
    }

}
