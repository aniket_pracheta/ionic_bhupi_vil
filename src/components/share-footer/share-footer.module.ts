import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ShareFooterComponent } from './share-footer';

@NgModule({
  declarations: [
    ShareFooterComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    ShareFooterComponent
  ]
})
export class ShareFooterComponentModule {}
