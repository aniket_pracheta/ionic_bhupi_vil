import {Component} from '@angular/core';
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {LogProvider} from '../../providers/log/log';

/**
 * Generated class for the ShareFooterComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
    selector: 'share-footer',
    templateUrl: 'share-footer.html'
})
export class ShareFooterComponent {

    text: string;

    constructor(public logProvider: LogProvider, public iab: InAppBrowser) {
        this.logProvider.log('ShareFooterComponent');
    }

    openInAppBrowser(url) {
        this.iab.create(url, "_blank", "location=no,zoom=no,toolbar=no");
    }

}
