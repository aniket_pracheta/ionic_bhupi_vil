import { DineAllPage } from '../pages/dine-all/dine-all';
import { PromotionPage } from '../pages/promotion/promotion';
import { StayRoomsPage } from '../pages/stay-rooms/stay-rooms';
import { DineDetailPage } from '../pages/dine-detail/dine-detail';
import { PromotionDetailPage } from '../pages/promotion-detail/promotion-detail';
import { StayRoomDetailsPage } from '../pages/stay-room-details/stay-room-details';
import { NotificationPage } from '../pages/notification/notification';
import { StayPackagesPage } from '../pages/stay-packages/stay-packages';

export var pageTemplate = {
    //'block_page_template':DineAllPage,
    // 'image_grid_template': PromotionPage,
    // 'list_view_template': StayRoomsPage,
    // 'DineDetailPage': DineDetailPage,
    // 'detail_without_image_template': PromotionDetailPage,
    // 'detail_with_image_template': PromotionDetailPage,
    // 'detail_with_carousel_template': StayRoomDetailsPage,
    // 'NotificationPage': NotificationPage,
    'image_grid_with_content_template' : StayPackagesPage,
}