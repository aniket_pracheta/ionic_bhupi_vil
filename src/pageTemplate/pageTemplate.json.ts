import { DineAllPage } from '../pages/dine-all/dine-all'; 
import { PromotionPage } from '../pages/promotion/promotion';
import { StayRoomsPage } from '../pages/stay-rooms/stay-rooms';
import { DineDetailPage } from '../pages/dine-detail/dine-detail';

import { StayPackagesPage } from '../pages/stay-packages/stay-packages';
import { StayRoomDetailsPage } from '../pages/stay-room-details/stay-room-details';
import { PromotionDetailPage } from '../pages/promotion-detail/promotion-detail';
import {StayAllPage} from "../pages/stay-all/stay-all";
import {MytestPage} from "../pages/mytest/mytest";
import {AboutPage} from "../pages/about/about";
import {MembershipInfoPage} from "../pages/membership-info/membership-info";
import {Component, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";


export var pageTemplate = {
    'block_page_template':DineAllPage,
    'dine_page_from_dashboard' : DineAllPage,
    'image_grid_template': PromotionPage,
    'stay_page_from_dashboard': MytestPage,
    'list_view_template': StayRoomsPage,
    'DineDetailPage' : DineDetailPage,
    'image_grid_with_content_template' : StayPackagesPage,
    'detail_without_image_template' : DineDetailPage,
    'detail_with_image_template' : PromotionDetailPage,
    // 'detail_with_carousel_template' : StayRoomDetailsPage,
    'sub_page_template' : DineDetailPage,
    'about_us_page_template': AboutPage,
    'member_benefits_vantage_template':MembershipInfoPage
}

