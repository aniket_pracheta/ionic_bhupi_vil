import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DineAllPage} from './dine-all';

@NgModule({
    declarations: [
        DineAllPage,
    ],
    imports: [
        IonicPageModule.forChild(DineAllPage),
    ],
    exports: [
        DineAllPage
    ]
})
export class DineAllPageModule {
}
