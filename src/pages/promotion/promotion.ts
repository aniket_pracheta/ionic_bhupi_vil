import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';

import {PromotionDetailPage} from '../promotion-detail/promotion-detail';

/* Http client provider */
import {HttpClientProvider} from '../../providers/http-client/http-client';

import {LogProvider} from '../../providers/log/log';

/* import Template */
import {pageTemplate} from '../../pageTemplate/pageTemplate.json';
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-promotion',
    templateUrl: 'promotion.html',
})
export class PromotionPage {
    containerHeight: any;
    promotionData: any = [];
    title: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, platform: Platform, public httpClient: HttpClientProvider, public logProvider: LogProvider, public fbaProvider: FbaProvider) {
        this.logProvider.log("PromotionPage");

        this.containerHeight = (platform.width() / 2);
        this.allPromotion(navParams.data.pageName);

        this.fbaProvider.postEvent('page_view', navParams.data.pageName);
    }

    /**
     * Name : allPromotion
     * Description : fetch featured block contain
     * @param page_name
     */
    allPromotion(page_name) {
        var data = {
            pageName: page_name
        }
        this.httpClient.subCategoryPage(data).then((data) => {
            this.title = data['data'].title; // set title of the page
            this.promotionData = data['data'].subdata; // set contain of the page
        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

    openPromotionDetail(promoData) {
        //  this.navCtrl.push(PromotionDetailPage);
        // this.navCtrl.push(pageTemplate[promoData.template],{
        //     pageName : promoData.page_to_be_linked // link page name
        // });
        this.navCtrl.push(PromotionDetailPage, {
            pageName: promoData.page_to_be_linked // link page name
        });
    }
}
