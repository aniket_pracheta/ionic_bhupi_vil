import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {StayRoomDetailsPage} from './stay-room-details';

@NgModule({
    declarations: [
        StayRoomDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(StayRoomDetailsPage),
    ],
    exports: [
        StayRoomDetailsPage
    ]
})
export class StayRoomDetailsPageModule {
}
