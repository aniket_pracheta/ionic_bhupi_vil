import {Component,ViewChild, ViewContainerRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/* Http client provider */
import { HttpClientProvider } from '../../providers/http-client/http-client';

import { LogProvider } from '../../providers/log/log';
import {ContainerRefs} from "../../global/components/container-refs";

/* import Template */
import {StayBookPage} from "../stay-book/stay-book";
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-stay-room-details',
    templateUrl: 'stay-room-details.html',
})
export class StayRoomDetailsPage {

    features:any;
    content : any;
    title : any;
    carouselData :any;
    capacity :any;
    pagerFlag = false;
    @ViewChild('container', { read: ViewContainerRef}) container: ViewContainerRef;

    constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient : HttpClientProvider, public logProvider : LogProvider, public containers: ContainerRefs, public fbaProvider: FbaProvider) {
        this.logProvider.log("StayRoomDetailsPage");
        this.subDetailPage(navParams.data.pageName);

        this.fbaProvider.postEvent('page_view', navParams.data.pageName);
    }

    /**
     * Name : subDetailPage
     * Description : fetch sub option detail page contain
     * @param page_name 
     */
    subDetailPage(page_name) {
        var data = {
            pageName: page_name
        }
        this.httpClient.subCategoryPage(data).then((data) => {
            this.title = data['data'][0].title;
            this.content = data['data'][0].content;
            this.containers.compileResponse(this.content, this.container);
            this.carouselData = data['data'][0].carousels;
            this.capacity = data['data'][0].capacity;
            this.features = data['data'][0].features
            if(data['data'][0].carousels.length == 1 ){
                this.pagerFlag = false;
            }else{
                this.pagerFlag = true;
            }
        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

    bookStayNow(){
        this.navCtrl.push(StayBookPage);
    }
}
