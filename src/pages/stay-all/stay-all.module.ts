import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {StayAllPage} from './stay-all';

@NgModule({
    declarations: [
        StayAllPage,
    ],
    imports: [
        IonicPageModule.forChild(StayAllPage),
    ],
    exports: [
        StayAllPage
    ]
})
export class StayAllPageModule {
}
