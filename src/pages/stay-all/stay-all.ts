import {Component,ViewChild, ViewContainerRef } from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

import {DineDetailPage} from '../dine-detail/dine-detail';

/* Http client provider */
import {HttpClientProvider} from '../../providers/http-client/http-client';
import {LogProvider} from '../../providers/log/log';

/* import Template */
import {pageTemplate} from '../../pageTemplate/pageTemplate.json';
import {ContainerRefs} from "../../global/components/container-refs";
import {StayBookPage} from "../stay-book/stay-book";
import {FbaProvider} from "../../providers/fba/fba";


@IonicPage()
@Component({
    selector: 'page-stay-all',
    templateUrl: 'stay-all.html',
})
export class StayAllPage {

    title: any;
    content: any;
    subPageData: any;
    pageTemp: any;
    @ViewChild('container', { read: ViewContainerRef}) container: ViewContainerRef;
    constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClientProvider, public logProvider: LogProvider, public containers: ContainerRefs, public fbaProvider: FbaProvider) {
        this.subCategoryPage(navParams.data.pageName); // call subCategoryPage() to fetch featured block contain

        this.fbaProvider.postEvent('page_view', navParams.data.pageName);
    }

    /**
     * Name : openDineDetail
     * Description : open sub option (dropdown menu) detail page
     * @param data
     */
    openDineDetail(data) {
        //API not providing template name, page_to_be_linked is also blank

        this.navCtrl.push(pageTemplate[data.template], {
            pageName: data.page_to_be_linked
        });

        // this.navCtrl.push(DineDetailPage,{
        //      pageName : data.page_to_be_linked
        // });
    }

    /**
     * Name : subCategoryPage
     * Description : fetch featured block contain
     * @param page_name
     */
    subCategoryPage(page_name) {
        var data = {
            pageName: page_name
        };
        this.httpClient.subCategoryPage(data).then((data) => {
            this.title = data['data'].title; // set title of the page
            this.content = data['data'].content; // set contain of the page
            this.subPageData = data['data'].nav; // set dropdown list (sub option)
            this.containers.compileResponse(this.content, this.container);
        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

    bookStayNow(){
        this.navCtrl.push(StayBookPage);
    }
}