import {Component, Input, ViewChild, ViewContainerRef} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {LogProvider} from '../../providers/log/log';
import {GlobalVarsProvider} from '../../providers/global-vars/global-vars';

import {HttpClientProvider} from "../../providers/http-client/http-client";
import {Storage} from "@ionic/storage";
import {PushNotification} from "../../global/components/push-notification";
import {ContainerRefs} from "../../global/components/container-refs";
import {AccountDetailPage} from '../account-detail/account-detail';
import {FbaProvider} from "../../providers/fba/fba";

export interface pushNotificationCount{
    count:any,
    apiStatus:any;

}

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    loginForm: any;
    submitAttempt: any;
    wrongDetails: any;
    device_token:any;
    device_type:any;
    title:any;
    content:any;
    info: pushNotificationCount = {count:0, apiStatus: ''};
    @ViewChild('container', { read: ViewContainerRef}) container: ViewContainerRef;
    constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public logProvider: LogProvider, public httpClient: HttpClientProvider, public toastCtrl: ToastController, private storage: Storage, public mobileInfo: PushNotification, public containers: ContainerRefs, public globalVarsProvider: GlobalVarsProvider, public fbaProvider: FbaProvider) {
        // this.device_token = this.mobileInfo.getDeviceToken();
        // this.device_type = this.mobileInfo.getDeviceType();
        //this.logProvider.logO("Login process  = ", this.device_token);
        this.loginForm = formBuilder.group({
            membership_num: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
            pin_num: ['', Validators.compose( [Validators.required])],
            device_token: [''],
            device_type: [''],
        });

        if (this.globalVarsProvider.isDevice) {
            this.device_token = this.mobileInfo.getDeviceToken();
            this.device_type = this.mobileInfo.getDeviceType();
        }

        this.fbaProvider.postEvent('page_view', 'Login');
    }

    ionViewDidLoad() {
        this.logProvider.log('ionViewDidLoad LoginPage');
        var data = {
            pageName: 'app-content-page?slug=login'
        }
        this.httpClient.subCategoryPage(data).then((data) => {
            this.logProvider.logO('Json data   ', (data['data'][0]['content'])) ;
            this.title = data['data'][0]['title'];
            this.content = data['data'][0]['content'];
            // this.tabs = data['data'][0]['tabs'];
            // this.featured_image = data['data'][0]['featured_image'];
            this.containers.compileResponse(this.content, this.container);
        }).catch((error) => {
            //this.logProvider.logO('Json data error  ', this.container);
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

    login() {
        this.submitAttempt = true;
        this.wrongDetails = false;
        if(this.loginForm.valid){

            var data = {
                pageName: 'app-member-login',
                dataSet: this.loginForm.value
            }
            this.httpClient.subCategoryPage(data, 'POST').then((data) => {
                if(data['status'] == 200){
                    this.storage.get('member_info').then((val) => {
                        if (val == '') {
                            this.storage.set('member_info', data);
                        } else {
                            this.storage.set('member_info', data);
                        }
                    });
                    this.mobileInfo.getApiStatus().then(value => {
                        this.info.apiStatus = value['status'];
                    }).catch(err=>{
                        this.info.apiStatus = 200;
                    });
                    this.mobileInfo.publishAnEvent('user:LoggedIn', data);
                    this.navCtrl.popToRoot();
                    this.navCtrl.push(AccountDetailPage);
                } else {

                    this.wrongDetails = true;

                }

            }).catch((error) => {
                this.logProvider.logO("Catch Error = ", JSON.stringify(error));
            });
        } else {
            this.logProvider.log('fail => '+ this.loginForm.value);
        }

    }

}
