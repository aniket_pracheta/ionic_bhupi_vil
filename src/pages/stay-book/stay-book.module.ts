import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StayBookPage } from './stay-book';

@NgModule({
  declarations: [
    StayBookPage,
  ],
  imports: [
    IonicPageModule.forChild(StayBookPage),
  ],
  exports: [
    StayBookPage
  ]
})
export class StayBookPageModule {}
