import {Component} from '@angular/core';
import {DatePipe} from '@angular/common';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LogProvider} from '../../providers/log/log';
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {FbaProvider} from "../../providers/fba/fba";

/**
 * Generated class for the StayBookPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-stay-book',
    templateUrl: 'stay-book.html',
})
export class StayBookPage {
    bookYourStayForm: any;
    roomTypes: any=[];
    date: any;
    submitAttempt: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private iab: InAppBrowser, public logProvider: LogProvider, private httpClient: HttpClientProvider, public datePipe: DatePipe, public fbaProvider: FbaProvider) {

        this.logProvider.log("StayBookPage");

        this.bookYourStayForm = formBuilder.group({
            // room_type: [''],
            date_in: ['', Validators.compose([Validators.required])],
            length: ['', Validators.compose([Validators.required])],
            adults: ['', Validators.compose([Validators.required])],
            children: ['', Validators.compose([Validators.required])]
        });
        this.date = new Date();

        this.fbaProvider.postEvent('page_view', 'Stay Book');

        // this.getRooms();
    }

    getRooms() {
        var data = {
            pageName: "app-room"
        };

        this.httpClient.subCategoryPage(data).then((data) => {
            var i = 0;
            for(let room of data['data'].subdata) {
                this.roomTypes.push(room.title);
                i++;
            }
        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

    showDatePicker() {
        this.bookYourStayForm.value.date_in = this.datePipe.transform(this.date, 'MM/dd/yy');
    }

    save() {
        this.submitAttempt = true;
        console.log(this.date);

        var URL = "https://bookings.ihotelier.com/Jupiters-Townsville-Hotel-And-Casino/bookings.jsp?DateIn=" + encodeURIComponent(this.datePipe.transform(this.bookYourStayForm.value.date_in, 'MM/dd/yy')) + "&Length=" + this.bookYourStayForm.value.length + "&Adults=" + this.bookYourStayForm.value.adults + "&Children=" + this.bookYourStayForm.value.children + "&HotelID=96903&LanguageID="+encodeURIComponent("#");

        this.logProvider.logO("URL", URL);

        if (this.bookYourStayForm.valid) {
            this.iab.create(URL, "_blank", "location=no,zoom=no,toolbar=no");
        }
    }
}
