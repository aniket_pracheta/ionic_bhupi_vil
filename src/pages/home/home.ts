import { Component } from '@angular/core';
import { NavController, } from 'ionic-angular';
import { PromotionPage } from '../promotion/promotion';
import { PromotionDetailPage } from '../promotion-detail/promotion-detail';
import { DineAllPage } from '../dine-all/dine-all';
import { StayAllPage } from '../stay-all/stay-all';
import { EventPage } from '../event/event';
import { LoginPage } from '../login/login';

/* Http client provider */
import { HttpClientProvider } from '../../providers/http-client/http-client';
import { GlobalVarsProvider } from '../../providers/global-vars/global-vars';
import { LogProvider } from '../../providers/log/log';
import {FbaProvider} from "../../providers/fba/fba";

/* Import Loader */
import { LoadingController } from 'ionic-angular';
/* import Template */
import { pageTemplate } from '../../pageTemplate/pageTemplate.json';



@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    sliderData : any;
    subCategory : any;
    homeArticle : any;
    pagerFlag = false;
    homeCarouselClassname : any;
    subCatClassname : any;
    homeUpdatesClassname : any;
    constructor(public navCtrl: NavController, public httpClient: HttpClientProvider, public globalVarsProvider: GlobalVarsProvider, public logProvider: LogProvider, public fbaProvider: FbaProvider) {
        this.fetchHomeCarousel(); // call fetchHomeCarousel() to fetch dashboard page data

        this.fbaProvider.postEvent('page_view', 'Home');
    }


    /**
     * Name :  openDineAll
     * Description : Open Sub category page like : stay, dine etc..
     * @param index 
     * @param data 
     */
    openDineAll(index,data) {
        this.logProvider.logO("data =>  ", pageTemplate);

        this.navCtrl.push(pageTemplate[data.template],{
            pageName : data.page_to_be_linked // link page name
        });
    }

    openStayAll() {
        this.navCtrl.push(StayAllPage);
    }

    bannerClick(bannerData) {
        this.logProvider.logO("banner = ", JSON.stringify(bannerData.template));

        this.navCtrl.push(pageTemplate[bannerData.template],{
            pageName : bannerData.page_to_be_linked // link page name
        });
    }

    openEventPage() {
        this.navCtrl.push(EventPage);
    }

    openPromotionListing() {
        this.navCtrl.push(PromotionPage);
    } 

    openLogin() {
        this.navCtrl.push(LoginPage);
    }

    homeArticleDetail(articleData){
        this.logProvider.log(articleData.template);
        this.navCtrl.push(pageTemplate[articleData.template],{
            pageName : articleData.page_to_be_linked // link page name
        });
    }

    /*
     * Name :  fetchHomeCarousel
     * Description : Call app-home api to fetch all dashboard/home page data
     */
    fetchHomeCarousel() {
        this.httpClient.homePageApi().then((productData) => {

            this.sliderData = productData['data'].carousel; // set carousel images
            this.homeCarouselClassname = 'carouselImage';
            this.subCatClassname = 'homeSubCat';
            this.homeUpdatesClassname = 'homeUpdates';
            // Show & Hide carousel pager
            if(productData['data'].carousel.length == 1 ){
                this.pagerFlag = false;
            }else{
                this.pagerFlag = true;
            }
            //end

            this.subCategory = productData['data'].featured_block; // set Sub Category block
            this.homeArticle = productData['data'].home_article; // set home article list
        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

}