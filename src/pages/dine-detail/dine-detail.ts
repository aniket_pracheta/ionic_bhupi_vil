import {AfterViewInit, Component, ViewChild, ViewContainerRef} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/* Http client provider */
import { HttpClientProvider } from '../../providers/http-client/http-client';

/* Template provider */
import { LogProvider } from '../../providers/log/log';
import {ContainerRefs} from "../../global/components/container-refs";
import {pageTemplate} from '../../pageTemplate/pageTemplate.json';
import {FbaProvider} from "../../providers/fba/fba";


@IonicPage()
@Component({
    selector: 'page-dine-detail',
    templateUrl: 'dine-detail.html',
})
export class DineDetailPage {
    showDetails: any = false;
    data: Array<{ title: string, details: string, icon: string, showDetails: boolean }> = [];
    title: any;
    content: any;
    featured_image: any;
    tabs: any;
    //subPageData :any;
    subPageData: Array<{ title: string, details: string, icon: string, showDetails: boolean }> = [];
    @ViewChild('container', { read: ViewContainerRef}) container: ViewContainerRef;

    constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClientProvider, public logProvider: LogProvider, public containers: ContainerRefs, public fbaProvider: FbaProvider) {
        this.logProvider.log("DineDetailPage "+ this.container);

        this.fbaProvider.postEvent('page_view', navParams.data.pageName);
    }

    ionViewDidLoad(){
        var data = {
            pageName: this.navParams.data.pageName
        }
        this.httpClient.subCategoryPage(data).then((data) => {
            this.title = data['data'][0]['title'];
            this.content = data['data'][0]['content'];
            this.tabs = data['data'][0]['tabs'];
            this.featured_image = data['data'][0]['featured_image'];
            this.containers.compileResponse(this.content, this.container);
        }).catch((error) => {
            this.logProvider.logO('Json data error  ', this.container);
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }
    /**
     * Name : subDetailPage
     * Description : fetch sub option detail page contain
     * @param page_name 
     */
    subDetailPage(page_name) {
        var data = {
            pageName: page_name
        }
        //this.logProvider.logO("DinePageName= ", data);
        this.httpClient.subCategoryPage(data).then((data) => {
            this.title = data['data'].title; // set title of page
            this.content = data['data'].content; // set contain of page
            //this.containers.compileResponse(this.content, this.container);
            this.logProvider.logO("content label= ", data['data'].tabs[0].label);
            this.logProvider.logO("this.subPageData = ", data['data'].tabs);

            // Show & Hide sub option page dropdown, hide a container if its contet not available for dropdown option
            if (data['data'].tabs[0].content == "") {
                this.subPageData = [];
            } else {
                data['data'].tabs.forEach(element => {
                    this.subPageData.push({
                        title: element.label,
                        details: element.content,
                        icon: 'ios-arrow-down',
                        showDetails: false
                    });
                });
            }
        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

    /**
     * Name : toggleDetails
     * Description : toggle dropdown menu by setting 'showDetails' flag
     * @param data 
     */
    toggleDetails(data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-down';
        } else {
            data.showDetails = true;
            data.icon = 'ios-arrow-up';
        }
    }
    isArray(val){
        if(val instanceof Array){
            return true;
        } else {
            return false;
        }

    }

    readMoreCurrentSpe(data){
        this.navCtrl.push(pageTemplate[data.template], {
            pageName: data.page_to_be_linked
        });
    }
}