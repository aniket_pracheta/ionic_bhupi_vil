import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DineDetailPage} from './dine-detail';

@NgModule({
    declarations: [
        DineDetailPage,
    ],
    imports: [
        IonicPageModule.forChild(DineDetailPage),
    ],
    exports: [
        DineDetailPage
    ]
})
export class DineDetailPageModule {
}