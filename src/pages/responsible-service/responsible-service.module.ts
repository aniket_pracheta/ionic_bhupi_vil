import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResponsibleServicePage } from './responsible-service';

@NgModule({
  declarations: [
    ResponsibleServicePage,
  ],
  imports: [
    IonicPageModule.forChild(ResponsibleServicePage),
  ],
  exports: [
    ResponsibleServicePage
  ]
})
export class ResponsibleServicePageModule {}
