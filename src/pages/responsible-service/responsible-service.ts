import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {LogProvider} from '../../providers/log/log';
import {FbaProvider} from "../../providers/fba/fba";

/**
 * Generated class for the ResponsibleServicePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-responsible-service',
    templateUrl: 'responsible-service.html',
})
export class ResponsibleServicePage {

    showDetails: any = false;

    data: Array<{ title: string, details: string, icon: string, showDetails: boolean }> = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public logProvider: LogProvider, public fbaProvider: FbaProvider) {
        this.logProvider.log("ResponsibleServicePage");

        var titles = ['Current Specials', 'Opening Hours', 'Prices', 'Where to find us', 'Bookings', 'Vantage Rewards'];

        var titles = ['Our commitment', 'Problem gambling', 'Getting help', 'Exclusions', 'Odds of winning', 'Customer complaints resolution', 'Minors are prohibited', 'Responsible service of alocohol', 'Passage of time', 'Financial transactions', 'Advertising and promotions', 'Customer liaison']
        for (let i = 0; i < 12; i++) {
            this.data.push({
                title: titles[i],
                details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                icon: 'ios-arrow-down',
                showDetails: false
            });
        }

        this.fbaProvider.postEvent('page_view', navParams.data.pageName);
    }

    ionViewDidLoad() {
        this.logProvider.log('ionViewDidLoad ResponsibleServicePage');
    }

    toggleDetails(data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-down';
        } else {
            data.showDetails = true;
            data.icon = 'ios-arrow-up';
        }
    }

}
