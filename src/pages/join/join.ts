import {Component, ViewChild, ViewContainerRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {LogProvider} from '../../providers/log/log';
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {ContainerRefs} from "../../global/components/container-refs";
import {FbaProvider} from "../../providers/fba/fba";

/**
 * Generated class for the JoinPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-join',
    templateUrl: 'join.html',
})
export class JoinPage {
    title:any;
    content:any;
    featured_image:any;
    @ViewChild('container', { read: ViewContainerRef}) container: ViewContainerRef;
    constructor(public navCtrl: NavController, public navParams: NavParams, public logProvider: LogProvider, public httpClient: HttpClientProvider, public containers: ContainerRefs, public fbaProvider: FbaProvider) {
        this.logProvider.log("JoinPage");

        this.fbaProvider.postEvent('page_view', 'Join Us');
    }

    ionViewDidLoad() {
        this.logProvider.log('ionViewDidLoad JoinPage');
        var data = {
            pageName: 'app-content-page?slug=join-us'
        }
        this.httpClient.subCategoryPage(data).then((data) => {
            this.logProvider.logO('Json data   ', (data['data'][0]['featured_image'])) ;
            this.title = data['data'][0]['title'];
            this.content = data['data'][0]['content'];
            this.featured_image = data['data'][0]['featured_image'];
            // // this.tabs = data['data'][0]['tabs'];
            // // this.featured_image = data['data'][0]['featured_image'];
            this.containers.compileResponse(this.content, this.container);
        }).catch((error) => {
            this.logProvider.logO('Json data error  ', this.container);
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

}
