import { Component,ViewChild, ViewContainerRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

/* Http client provider */
import { HttpClientProvider } from '../../providers/http-client/http-client';

import { LogProvider } from '../../providers/log/log';

/* import Template */
import { pageTemplate } from '../../pageTemplate/pageTemplate.json';
import { PromotionDetailPage } from '../promotion-detail/promotion-detail';
import {ContainerRefs} from "../../global/components/container-refs";
import {FbaProvider} from "../../providers/fba/fba";


@IonicPage()
@Component({
    selector: 'page-stay-packages',
    templateUrl: 'stay-packages.html',
})
export class StayPackagesPage {
    containerHeight: any;
    promotionData: any = [];
    imageData : any;
    title : any;
    content : any;
    @ViewChild('container', { read: ViewContainerRef}) container: ViewContainerRef;
    constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public httpClient: HttpClientProvider, public logProvider: LogProvider, public containers: ContainerRefs, public fbaProvider: FbaProvider) {
        this.logProvider.log("StayPackagesPage"+ navParams.data.pageName);

        //this.containerHeight = (platform.width() / 2);
        this.subDetailPage(navParams.data.pageName);

        this.fbaProvider.postEvent('page_view', navParams.data.pageName);
    }

    /**
     * Name : subDetailPage
     * Description : fetch sub option detail page contain
     * @param page_name 
     */
    subDetailPage(page_name) {
        var data = {
            pageName: page_name
        }
        this.httpClient.subCategoryPage(data).then((data) => {
            this.logProvider.logO("data['data'].featured_image = ", JSON.stringify(data['data'].featured_image));
            this.imageData = data['data'].featured_image;
            this.title = data['data'].title;
            this.content = data['data'].content;
            this.promotionData = data['data'].subdata;
            this.containers.compileResponse(this.content, this.container);

        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

    openPromotionDetail(pData){
        this.navCtrl.push(PromotionDetailPage,{
             pageName : pData.page_to_be_linked
        });
        this.logProvider.logO("Google", pData);
        // this.navCtrl.push(pageTemplate[pData.template],{
        //      pageName : pData.page_to_be_linked
        // });

    }

}