import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {StayPackagesPage} from './stay-packages';

@NgModule({
    declarations: [
        StayPackagesPage,
    ],
    imports: [
        IonicPageModule.forChild(StayPackagesPage),
    ],
    exports: [
        StayPackagesPage
    ]
})
export class PromotionPageModule {
}
