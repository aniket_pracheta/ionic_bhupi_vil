import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

import {LogProvider} from "../../providers/log/log";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {PushNotification} from "../../global/components/push-notification";
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-notification',
    templateUrl: 'notification.html',
})
export class NotificationPage {
    notificationArr: any = [];
    constructor(public navCtrl: NavController, public navParams: NavParams, public logProvider: LogProvider, public httpClient: HttpClientProvider, public pushNotification: PushNotification, public fbaProvider: FbaProvider) {
        this.pushNotification.getListOfNotification().then(value => {
            this.notificationArr = value;
                this.logProvider.log("NotificationPage" + value);
        });

        this.fbaProvider.postEvent('page_view', 'Notification');
    }

    showNotificationInAlert(data){
        var returnData =  this.pushNotification.showPushNotificationAlert(data.title, data.content, data.id, data.template, data.page_to_be_linked, this.navCtrl);
        this.logProvider.log(`id ${data.id} title ${data.title} content ${data.content}`);
        //this.getListOfNotification();

    }


}
