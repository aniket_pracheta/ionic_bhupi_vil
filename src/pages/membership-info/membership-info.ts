import {Component, ViewChild, ViewContainerRef} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

import {LogProvider} from '../../providers/log/log';
import {ContainerRefs} from "../../global/components/container-refs";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {FbaProvider} from "../../providers/fba/fba";

/**
 * Generated class for the MembershipInfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-membership-info',
    templateUrl: 'membership-info.html',
})
export class MembershipInfoPage {
    backc: boolean = false;
    title: any;
    content: any;

    @ViewChild('container', { read: ViewContainerRef}) container: ViewContainerRef;
    constructor(public navCtrl: NavController, public navParams: NavParams, public logProvider: LogProvider, public containers: ContainerRefs, public httpClient: HttpClientProvider, public fbaProvider: FbaProvider) {
        //this.containers.compileResponse(this.content, this.container);
        this.logProvider.log("MembershipInfoPage");

        this.fbaProvider.postEvent('page_view', 'Member Benefit');
    }

    ionViewDidLoad() {
        this.logProvider.log('ionViewDidLoad MembershipInfoPage');
        var data = {
            pageName: 'app-member-benefits'
        }
        this.httpClient.subCategoryPage(data).then((data) => {
            this.logProvider.logO('Json data   ', JSON.stringify(data)) ;
            this.title = data['data']['title'];
            this.content = data['data']['content'];
            // this.tabs = data['data'][0]['tabs'];
            // this.featured_image = data['data'][0]['featured_image'];
            this.containers.compileResponse(this.content, this.container);
        }).catch((error) => {
            this.logProvider.logO('Json data error  ', this.container);
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

    tabContent() {

    }

}
