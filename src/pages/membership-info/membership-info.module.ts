import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MembershipInfoPage } from './membership-info';

@NgModule({
  declarations: [
    MembershipInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(MembershipInfoPage),
  ],
  exports: [
    MembershipInfoPage
  ]
})
export class MembershipInfoPageModule {}
