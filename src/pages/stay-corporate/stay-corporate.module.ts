import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {StayCorporatePage} from './stay-corporate';

@NgModule({
    declarations: [
        StayCorporatePage,
    ],
    imports: [
        IonicPageModule.forChild(StayCorporatePage),
    ],
    exports: [
        StayCorporatePage
    ]
})
export class DineDetailPageModule {
}
