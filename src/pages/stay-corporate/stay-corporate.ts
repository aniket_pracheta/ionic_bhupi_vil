import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {LogProvider} from '../../providers/log/log';
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-stay-corporate',
    templateUrl: 'stay-corporate.html',
})
export class StayCorporatePage {

    data: Array<{ title: string, details: string, icon: string, showDetails: boolean }> = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public logProvider: LogProvider, public fbaProvider: FbaProvider) {
        this.logProvider.log("StayCorporatePage");

        var titles = ['Current Specials', 'Opening Hours', 'Prices', 'Where to find us', 'Bookings', 'Vantage Rewards'];
        for (let i = 0; i < 5; i++) {
            this.data.push({
                title: titles[i],
                details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                icon: 'ios-arrow-down',
                showDetails: false
            });
        }

        this.fbaProvider.postEvent('page_view', 'Stay Corporate');
    }

    toggleDetails(data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-down';
        } else {
            data.showDetails = true;
            data.icon = 'ios-arrow-up';
        }
    }
}