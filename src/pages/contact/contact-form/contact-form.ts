import {Component, Input} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LogProvider} from '../../../providers/log/log';
import {HttpClientProvider} from "../../../providers/http-client/http-client";

@Component({
    selector: 'page-contact-form',
    templateUrl: 'contact-form.html',
})
export class ContactFormPage {
    contactForm: any;
    submitAttempt: any;
    selectOptions: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public logProvider: LogProvider, private httpClient: HttpClientProvider, public toastCtrl: ToastController) {

        this.logProvider.log("ContactFormPage");

        this.contactForm = formBuilder.group({
            enquiry_type: [''],
            name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            telephone: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required, Validators.pattern("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,4})+$")])],
            enquiry: ['', Validators.compose([Validators.required])],
        });
        this.selectOptions = {
            cssClass:'customBtnColor'
        };
    }

    save(contact) {
        this.submitAttempt = true;

        if (!this.contactForm.valid) {
            // this.signupSlider.slideTo(0);
        }
        else if (!this.contactForm.valid) {
            // this.signupSlider.slideTo(1);
        }
        else {
            var data = {
                pageName: contact,
                dataSet: this.contactForm.value
            }
            this.httpClient.subCategoryPage(data, 'POST').then((data) => {
                this.contactForm.reset();
                //console.log(this.contactForm.value);
                let toast: any;
                if(data['status'] == true){
                    toast = this.toastCtrl.create({
                        message: 'Request was sent successfully',
                        duration: 3000,
                        position: 'bottom'
                    });
                } else {

                    toast = this.toastCtrl.create({
                        message: 'Internal server error',
                        duration: 3000,
                        position: 'bottom'
                    });

                }
                toast.present();
                this.logProvider.logO("contact Us = ", data['status']);
            }).catch((error) => {
                this.logProvider.logO("Catch Error = ", JSON.stringify(error));
            });
            //this.logProvider.log(this.contactForm.value);
        }
    }
}