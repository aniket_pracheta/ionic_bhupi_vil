import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';


/* Http client provider */
import {HttpClientProvider} from '../../providers/http-client/http-client';
import {LogProvider} from '../../providers/log/log';

/* Import Loader */
import {LoadingController} from 'ionic-angular';
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-contact',
    templateUrl: 'contact.html',
})
export class ContactPage {
    showDetails: any = false;
    locationShowDetails: any = false;
    contactData: any;
    latlongData: any;
    icon: any = "ios-arrow-down";
    locationicon: any = "ios-arrow-down";
    data: Array<{ title: string, details: string, icon: string, showDetails: boolean }> = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClientProvider, public logProvider: LogProvider, public fbaProvider: FbaProvider) {
        this.logProvider.log("ContactPage");
        this.fetchContactDetail();

        this.fbaProvider.postEvent('page_view', 'Contact Us');
    }

    fetchContactDetail() {
        this.httpClient.contactDetail().then((data) => {
            this.latlongData = {
                lat: data['data'].location.lat,
                long: data['data'].location.long,
                address: data['data'].location.address,
            };

            var titles = ['General Enquiries', 'Events', 'Bookings'];
            var detail = [data['data'].general_enquiries, data['data'].events, data['data'].bookings]
            for (let i = 0; i < 3; i++) {
                this.data.push({
                    title: titles[i],
                    details: detail[i],
                    icon: 'ios-arrow-down',
                    showDetails: false
                });
            }

            // this.contactData = productData['data'].carousel;
        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

    toggleDetails(data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-down';
        } else {
            data.showDetails = true;
            data.icon = 'ios-arrow-up';
        }
    }

    toggleContactForm(data) {
        if (this.showDetails) {
            this.showDetails = false;
            this.icon = 'ios-arrow-down';
        } else {
            this.showDetails = true;
            this.icon = 'ios-arrow-up';
        }
    }

    toggleLocation(data) {
        if (this.locationShowDetails) {
            this.locationShowDetails = false;
            this.locationicon = 'ios-arrow-down';
        } else {
            this.locationShowDetails = true;
            this.locationicon = 'ios-arrow-up';
        }
    }

}