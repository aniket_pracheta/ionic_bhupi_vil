import {Component, ViewChild, Input} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {LogProvider} from '../../../providers/log/log';

import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    LatLng,
    CameraPosition,
    MarkerOptions,

} from '@ionic-native/google-maps';
/**
 * Generated class for the LocationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
    selector: 'page-location',
    templateUrl: 'location.html',
})
export class LocationPage {
    @Input() latlong: any;
    @ViewChild('map') mapElement;
    map: any;
    lat: any;
    lng: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private googleMaps: GoogleMaps, public logProvider: LogProvider) {
        this.logProvider.log("LocationPage");
    }

    ngAfterViewInit() {
        this.initMap(this.latlong.lat, this.latlong.long);
    }


    initMap(latitude, longitude) {
        let latlang = new google.maps.LatLng(latitude, longitude);
        let mapOption = {
            center: latlang,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOption)

        let marker = new google.maps.Marker({
            map: this.map,
            icon: "assets/img/icon_locationServices.png",
            position: this.map.getCenter()
        });

        let content = "<h6>" + this.latlong.address + "</h6>";

        this.showInfo(marker, content);


    }

    showInfo(marker, content) {
        let info = new google.maps.InfoWindow({
            content: content
        });

        google.maps.event.addListener(marker, 'click', function () {
            info.open(this.map, marker);

        });

    }

    ionViewDidLoad() {
        this.logProvider.log('ionViewDidLoad LocationPage');
    }


    loadMap() {
        this.logProvider.log("inside load map");
        // make sure to create following structure in your view.html file
        // and add a height (for example 100%) to it, else the map won't be visible
        // <ion-content>
        //  <div #map id="map" style="height:100%;"></div>
        // </ion-content>

        // create a new map by passing HTMLElement
        let element: HTMLElement = document.getElementById('map');

        let map: GoogleMap = this.googleMaps.create(element);

        // listen to MAP_READY event
        // You must wait for this event to fire before adding something to the map or modifying it in anyway
        map.one(GoogleMapsEvent.MAP_READY).then(
            () => {
                this.logProvider.log('Map is ready!');
                // Now you can add elements to the map like the marker
            }
        );

        // create LatLng object
        let ionic: LatLng = new LatLng(43.0741904, -89.3809802);

        // create CameraPosition
        let position: CameraPosition = {
            target: ionic,
            zoom: 18,
            tilt: 30
        };

        // move the map's camera to position
        map.moveCamera(position);

        // create new marker
        let markerOptions: MarkerOptions = {
            position: ionic,
            icon: "assets/img/logo.png",
            title: 'Ionic'
        };

    }
}
