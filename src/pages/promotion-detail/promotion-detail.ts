import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/* Http client provider */
import { HttpClientProvider } from '../../providers/http-client/http-client';
import { LogProvider } from '../../providers/log/log';
import {FbaProvider} from "../../providers/fba/fba";


@IonicPage()
@Component({
    selector: 'page-promotion-detail',
    templateUrl: 'promotion-detail.html',
})
export class PromotionDetailPage {

    imageData: any;
    imageFlag : boolean = false;
    title : string;
    content : any;
    services: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient : HttpClientProvider, public logProvider : LogProvider, public fbaProvider: FbaProvider) {
        this.logProvider.log("PromotionDetailPage");
        this.subDetailPage(navParams.data.pageName);

        this.fbaProvider.postEvent('page_view', navParams.data.pageName);
    }

    
    /**
     * Name : subDetailPage
     * Description : fetch sub option detail page contain
     * @param page_name 
     */
    subDetailPage(page_name) {
        var data = {
            pageName: page_name
        }
        this.httpClient.subCategoryPage(data).then((data) => {
            this.imageData = data['data'][0].featured_image || data['data'][0].banner;
            this.logProvider.logO("image =", data['data'][0].banner);
            if( (data['data'][0].featured_image || data['data'][0].banner) == undefined || (data['data'][0].featured_image || data['data'][0].banner) == null || (data['data'][0].featured_image || data['data'][0].banner) == false){
                this.imageFlag = false;
            }else{
                this.imageFlag = true;
            }
            this.title = data['data'][0].title;
            this.content = data['data'][0].content;
            this.services = data['data'][0].services;
        
        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }
}
