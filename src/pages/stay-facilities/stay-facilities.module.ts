import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {StayFacilitiesPage} from './stay-facilities';

@NgModule({
    declarations: [
        StayFacilitiesPage,
    ],
    imports: [
        IonicPageModule.forChild(StayFacilitiesPage),
    ],
    exports: [
        StayFacilitiesPage
    ]
})
export class DineDetailPageModule {
}
