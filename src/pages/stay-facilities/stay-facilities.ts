import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {LogProvider} from '../../providers/log/log';
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-stay-facilities',
    templateUrl: 'stay-facilities.html',
})
export class StayFacilitiesPage {
    facilitiesData:any;
    data: Array<{ title: string, details: string, icon: string, showDetails: boolean }> = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public logProvider: LogProvider, public fbaProvider: FbaProvider) {
        this.logProvider.log("StayFacilitiesPage");

        this.fbaProvider.postEvent('page_view', 'Stay Facilities');

        this.facilitiesData = ['Free Wi-Fi','Currency exchange','Luggage storage','Message service','Voice mail facilities','Postal services','Valet parking','Pre-registration and express check-out facilities','Wheelchairs available','Universal adaptors and power converters','In-room dining']
        var titles = ['Current Specials', 'Opening Hours', 'Prices', 'Where to find us', 'Bookings', 'Vantage Rewards'];
        for (let i = 0; i < 5; i++) {
            this.data.push({
                title: titles[i],
                details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                icon: 'ios-arrow-down',
                showDetails: false
            });
        }
    }

    toggleDetails(data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-down';
        } else {
            data.showDetails = true;
            data.icon = 'ios-arrow-up';
        }
    }
}
