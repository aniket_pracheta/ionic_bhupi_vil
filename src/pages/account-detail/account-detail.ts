import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

import {LogProvider} from '../../providers/log/log';
import {Storage} from "@ionic/storage";
import {LoginPage} from "../login/login";
import {MembershipInfoPage} from "../membership-info/membership-info";
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-account-detail',
    templateUrl: 'account-detail.html',
})
export class AccountDetailPage {

    hotelShowDetails: any = false;
    offerFlag: any = false;
    hotelShowDetailsIcon: any = "ios-arrow-down";
    offerIcon: any = "ios-arrow-down";
    accountInfo: any = [];
    allMemberInfo:any;
    data: Array<{ title: string, details: string, btnTitle: string, showDetails: boolean }> = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public logProvider: LogProvider, public storage: Storage, public fbaProvider: FbaProvider) {
        this.logProvider.log("AccountDetailPage");

        this.fbaProvider.postEvent('page_view', 'Vantage Rewards');

        this.storage.get('member_info').then((val) => {
            this.accountInfo = val.data;
            //this.allMemberInfo =  this.transformHtml(this.accountInfo[0]['all_member_benefits']['content'], 200)
            this.allMemberInfo =  this.accountInfo[0]['all_member_benefits']['content'];
        }).catch((error) => {
            this.accountInfo = [];
            console.log("Catch Error login = ", JSON.stringify(error));
        });
        var titles = ['$10 OFF AT SPIN', 'FREE COFFEE AT AQUA'];
        for (let i = 0; i < 2; i++) {
            this.data.push({
                title: titles[i],
                details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                btnTitle: 'BOOK NOW AT SPIN',
                showDetails: false

            });
        }
    }

    toggleHotelDetails() {
        if (this.hotelShowDetails) {
            this.hotelShowDetails = false;
            this.hotelShowDetailsIcon = 'ios-arrow-down';
        } else {
            this.hotelShowDetails = true;
            this.hotelShowDetailsIcon = 'ios-arrow-up';
        }
    }

    toggleOffer() {
        if (this.offerFlag) {
            this.offerFlag = false;
            this.offerIcon = 'ios-arrow-down';
        } else {
            this.offerFlag = true;
            this.offerIcon = 'ios-arrow-up';
        }
    }

    goToLoginPage(){
        this.navCtrl.push(LoginPage);
    }

    transformHtml(value: string, args: any) : string {
        let limit = args > 0 ? parseInt(args, 10) : 10;
        let trail = args > 1 ? args + '...' : '...';

        return value.length > limit ? value.substring(0, limit) + trail : value;
    }

    goToAllMemberInfo(page_to_be_linked){
        this.navCtrl.push(MembershipInfoPage, {
            pageName: page_to_be_linked
        });
    }

}
