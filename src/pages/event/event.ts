import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';

import {EventDetailPage} from '../event-detail/event-detail';

import {LogProvider} from '../../providers/log/log';
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-event',
    templateUrl: 'event.html',
})
export class EventPage {
    containerHeight: any;
    eventsData: any = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, platform: Platform, public logProvider: LogProvider, public fbaProvider: FbaProvider) {
        this.logProvider.log("EventPage");

        this.fbaProvider.postEvent('page_view', "Events");

        this.containerHeight = (platform.width() / 2);
        this.eventsData = [
            {src: "http://www.the-ville.com.au/wp-content/uploads/2016/12/square-tile_467x467_plunge-1.jpg"},
            {src: "http://freedesignfile.com/upload/2016/06/Promotion-comic-speech-bubble-vector.jpg"},
            {src: "http://images.locanto.com.au/1935348381/Random-Rewards-at-The-Ville_1.jpg"},
            {src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuvFFG-tWm-ouHRhzj_obW4LNZlXHq6h2E7yVFYCM-0zeI0Nvq"},
            {src: "http://www.the-ville.com.au/wp-content/uploads/2017/05/VIL50821_WINTER-CASH-CLIMB_square-tile_467x467_MAY17_GOLD.jpg"},
            {src: "https://thumb9.shutterstock.com/display_pic_with_logo/1453121/551317036/stock-vector-vector-logo-black-jack-three-playing-cards-different-suits-for-gambling-game-blackjack-chips-551317036.jpg"},
            {src: "http://freedesignfile.com/upload/2016/06/Promotion-comic-speech-bubble-vector.jpg"},
            {src: "http://www.the-ville.com.au/wp-content/uploads/2017/02/VIL50686_MEMBER-DRAW_square-tile_467x467_FEB17.jpg"},
            {src: "http://images.locanto.com.au/1935348381/Random-Rewards-at-The-Ville_1.jpg"},
            {src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuvFFG-tWm-ouHRhzj_obW4LNZlXHq6h2E7yVFYCM-0zeI0Nvq"},
            {src: "http://www.the-ville.com.au/wp-content/uploads/2017/05/VIL50821_WINTER-CASH-CLIMB_square-tile_467x467_MAY17_GOLD.jpg"},
            {src: "https://thumb9.shutterstock.com/display_pic_with_logo/1453121/551317036/stock-vector-vector-logo-black-jack-three-playing-cards-different-suits-for-gambling-game-blackjack-chips-551317036.jpg"}
        ];
    }

    openEventDetail(data) {
        this.navCtrl.push(EventDetailPage);
    }
}