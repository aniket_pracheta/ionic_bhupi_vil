import {Component,ViewChild, ViewContainerRef } from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

import {LogProvider} from '../../providers/log/log';

import {HttpClientProvider} from '../../providers/http-client/http-client';
import {ContainerRefs} from "../../global/components/container-refs";
import {pageTemplate} from '../../pageTemplate/pageTemplate.json';
import {FbaProvider} from "../../providers/fba/fba";


@IonicPage()
@Component({
    selector: 'page-about',
    templateUrl: 'about.html',
})
export class AboutPage {
    showDetails: any = false;
    data: Array<{ title: string, details: string, icon: string, showDetails: boolean }> = [];
    title: any;
    content:any;
    aboutUpdate: any = [];
    privacyInfo: any = [];
    @ViewChild('container', { read: ViewContainerRef}) container: ViewContainerRef;
    constructor(public navCtrl: NavController, public navParams: NavParams, public logProvider: LogProvider, private httpClient: HttpClientProvider, public containers: ContainerRefs, public fbaProvider: FbaProvider) {
        this.logProvider.log("AboutPage");

        this.aboutPageInfo(navParams.data.pageName);

        this.fbaProvider.postEvent('page_view', 'About');

        var titles = ['Compliance with privacy legistation', 'Collection of personal information', 'Use of personal information', 'Disclouser of personal information', 'Sharing information with others', 'Marketing'];
        for (let i = 0; i < titles.length; i++) {
            this.data.push({
                title: titles[i],
                details: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                icon: 'ios-arrow-down',
                showDetails: false
            });
        }

        this.fbaProvider.postEvent('page_view', 'home');
    }

    /**
     * Name : allPromotion
     * Description : fetch featured block contain
     * @param page_name
     */
    aboutPageInfo(page_name) {
        var data = {
            pageName: page_name
        }
        this.httpClient.subCategoryPage(data).then((data) => {
            this.title = data['data'].aboutus.title;
            this.content = data['data'].aboutus.content;
            this.aboutUpdate = data['data'].updates;
            this.privacyInfo = data['data'].privacy;
            this.containers.compileResponse(this.content, this.container);

            this.logProvider.logO("response = ", data['data'].aboutus.title);
        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }

    toggleDetails(data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-down';
        } else {
            data.showDetails = true;
            data.icon = 'ios-arrow-up';
        }
    }

    openAboutUpdates(data){
        this.navCtrl.push(pageTemplate[data.template], {
            pageName: data.page_to_be_linked
        });
    }
}