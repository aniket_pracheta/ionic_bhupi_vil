import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { StayRoomDetailsPage } from '../stay-room-details/stay-room-details';

/* Http client provider */
import { HttpClientProvider } from '../../providers/http-client/http-client';

import { LogProvider } from '../../providers/log/log';

/* import Template */
//import { pageTemplate } from '../../pageTemplate/test';
import { pageTemplate } from '../../pageTemplate/pageTemplate.json';
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-stay-rooms',
    templateUrl: 'stay-rooms.html',
})
export class StayRoomsPage {

    // data: Array<{ title: string, details: string, icon: string, showDetails: boolean }> = [];
    roomData: any;
    title: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClientProvider, public logProvider: LogProvider, public fbaProvider: FbaProvider) {
        this.logProvider.log("StayRoomsPage" + navParams.data.pageName);
        this.roomListing(navParams.data.pageName);

        this.fbaProvider.postEvent('page_view', navParams.data.pageName);
    }


    roomDetail(index, room_data) {
        this.navCtrl.push(StayRoomDetailsPage, {
            pageName: room_data.page_to_be_linked
        });

        // this.navCtrl.push(pageTemplate[room_data.template],{
        //     pageName : room_data.page_to_be_linked // link page name
        // });
     //   console.log("pageTemplate[room_data.template] = "+this.httpClient.pageTemplate['detail_with_carousel_template']);
        this.logProvider.logO("data.template =", room_data.template);
        this.logProvider.logO("page_to_be_linked = ", room_data.page_to_be_linked);
        //  console.log("test ="+pageTemplate['block_page_template']);
        //   this.navCtrl.push(pageTemplate['block_page_template']);
    }

    /**
     * Name : roomListing
     * Description : fetch sub option detail page contain
     * @param page_name 
     */
    roomListing(page_name) {
        var data = {
            pageName: page_name
        }
        this.httpClient.subCategoryPage(data).then((data) => {
            this.title = data['data'].title;
            this.roomData = data['data'].subdata;
            
        }).catch((error) => {
            this.logProvider.logO("Catch Error = ", JSON.stringify(error));
        });
    }


}