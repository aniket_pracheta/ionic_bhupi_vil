import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {StayRoomsPage} from './stay-rooms';

@NgModule({
    declarations: [
        StayRoomsPage,
    ],
    imports: [
        IonicPageModule.forChild(StayRoomsPage),
    ],
    exports: [
        StayRoomsPage
    ]
})
export class DineDetailPageModule {
}
