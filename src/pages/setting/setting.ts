import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

import {LogProvider} from '../../providers/log/log';
import {PushNotification} from "../../global/components/push-notification";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-setting',
    templateUrl: 'setting.html',
})
export class SettingPage {

    public isPushNotificationOn: boolean;
    public isEmailSubs: boolean;
    public isLocationService: boolean;
    public toggleName: any;
    patron_number:any;
    device_type_name:any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public logProvider: LogProvider, public pushNotification: PushNotification, public httpClient: HttpClientProvider, public fbaProvider: FbaProvider) {
        this.logProvider.log("SettingPage");
        this.device_type_name = this.pushNotification.getDeviceType();

        this.fbaProvider.postEvent('page_view', 'Settings');

        this.pushNotification.getMemberId().then((data) => {
            if(data == undefined){

            } else {
                this.patron_number = data;
                this.getUserSettings();
            }
        }).catch((err)=>{

        });

    }
    notify(dataset,toggleVal) {
        console.log(dataset, toggleVal);
        var dataParam: any;
        var toggleValue =  (toggleVal == true) ? 1: 0;
        this.pushNotification.getMemberId().then((data) => {
            if(data == undefined){
                this.logProvider.logO('ID undefined->', data);
            } else {
                if (dataset == 'isPushNotificationOn'){
                    this.toggleName = 'push_notification';
                     dataParam = {pageName: 'app-update-notification-settings', dataSet: {"patron_number": this.patron_number, "device_type":this.device_type_name, 'push_notification': toggleValue}}
                } else if (dataset == 'isEmailSubs'){
                    this.toggleName = 'email_subscription';
                     dataParam = {pageName: 'app-update-notification-settings', dataSet: {"patron_number": this.patron_number, "device_type":this.device_type_name, 'email_subscription': toggleValue}}
                } else {
                    this.toggleName = 'location_services';
                     dataParam = {pageName: 'app-update-notification-settings', dataSet: {"patron_number": this.patron_number, "device_type":this.device_type_name, 'location_services': toggleValue}}
                }

                this.httpClient.subCategoryPage(dataParam, 'POST', false).then((data) => {
                    console.log(JSON.stringify(data));
                    this.isPushNotificationOn = data['data'][0]['push_notification'];
                    this.isEmailSubs = data['data'][0]['email_subscription'];
                    this.isLocationService = data['data'][0]['location_services'];
                }).catch((error) => {

                });
            }
        });
    }

    getUserSettings(){
        var data = {
            pageName: 'app-get-notification-settings',
            dataSet: {"patron_number": this.patron_number, "device_type":this.device_type_name}
        };
        this.httpClient.subCategoryPage(data, 'POST', false).then((data) => {
            console.log(JSON.stringify(data));
            this.isPushNotificationOn = data['data'][0]['push_notification'];
            this.isEmailSubs = data['data'][0]['email_subscription'];
            this.isLocationService = data['data'][0]['location_services'];
        }).catch((error) => {

        });
    }

}
