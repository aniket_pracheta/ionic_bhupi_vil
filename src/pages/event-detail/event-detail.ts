import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {LogProvider} from '../../providers/log/log';
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-event-detail',
    templateUrl: 'event-detail.html',
})
export class EventDetailPage {
    containerHeight: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public logProvider: LogProvider, public fbaProvider: FbaProvider) {
        this.logProvider.log("EventDetailPage");

        this.fbaProvider.postEvent('page_view', "Events Detail");
        this.containerHeight = (platform.width() / 2);
    }
}