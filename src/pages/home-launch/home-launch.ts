import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, MenuController, Slides, ToastController} from 'ionic-angular';
import {LogProvider} from '../../providers/log/log';


import {Storage} from '@ionic/storage';
import {HomePage} from '../home/home';
import {Network} from "@ionic-native/network";
import {FbaProvider} from "../../providers/fba/fba";

@IonicPage()
@Component({
    selector: 'page-home-launch',
    templateUrl: 'home-launch.html',
})
export class HomeLaunchPage {
    showSkip = true;
    @ViewChild('slides') slides: Slides;

    constructor(public navCtrl: NavController, public menu: MenuController, public storage: Storage, public logProvider: LogProvider, public network:Network, public toastCtrl: ToastController, public fbaProvider: FbaProvider) {
        this.logProvider.logO("HomeLaunchPage", this.network);

        this.fbaProvider.postEvent('page_view', 'Home Launch');

        this.network.onDisconnect().subscribe(() => {
            // let toast = this.toastCtrl.create({
            //     message: 'Internet Connection Lost',
            //     duration: 3000,
            //     position: 'middle'
            // });
            //
            // toast.present();
            this.logProvider.log('network was disconnected (123)');
        });
    }

    startApp() { 
        this.navCtrl.setRoot(HomePage).then(() => {
            this.storage.set('hasSeenLaunch', 'true');
        })
    }

    onSlideChangeStart(slider: Slides) {
        this.showSkip = !slider.isEnd();
    }

    ionViewWillEnter() {
        this.slides.update();
    }

    ionViewDidEnter() {
        // the root left menu should be disabled on the tutorial page
        this.menu.enable(false);
    }

    ionViewDidLeave() {
        // enable the root left menu when leaving the tutorial page
        this.menu.enable(true, 'loggedOutMenu');
    }

}