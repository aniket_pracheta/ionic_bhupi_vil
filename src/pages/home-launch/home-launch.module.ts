import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {HomeLaunchPage} from './home-launch';

@NgModule({
    declarations: [
        HomeLaunchPage,
    ],
    imports: [
        IonicPageModule.forChild(HomeLaunchPage),
    ],
    exports: [
        HomeLaunchPage
    ]
})
export class HomeLaunchPageModule {
}
