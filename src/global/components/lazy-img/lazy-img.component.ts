import { Component, Input } from '@angular/core';

/**
 * Component in charge of lazy load images and cache them
 */
@Component({
  selector: 'lazy-img',
  template: `
  <div text-center>
    <img [inputSrc]="inputSrc" [imgClass]="imgClass" lazy-load (loaded)="placeholderActive = false"/>
  </div>
  `
})
export class LazyImgComponent {

    @Input() inputSrc: string;
    @Input() imgClass: any;

  public placeholderActive: boolean = true;

}
