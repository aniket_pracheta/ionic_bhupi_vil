import {Compiler, Component, Injectable, NgModule , ComponentRef, ModuleWithComponentFactories } from '@angular/core';

import {CommonModule} from "@angular/common";
import {NavController} from "ionic-angular";
import {pageTemplate} from "../../pageTemplate/pageTemplate.json";

@Injectable()
export  class ContainerRefs{
    private dynamicComponentRefs: ComponentRef<any>;
    constructor(public compiler: Compiler){}

    compileResponse(content, container){
        this.compiler.compileModuleAndAllComponentsAsync(this.createDynamicComponent(content))
            .then((mwcf: ModuleWithComponentFactories<any>) => {
                let factory = mwcf.componentFactories.find(cf =>
                    cf.componentType.name === 'DynamicComponent');
                this.dynamicComponentRefs = container.createComponent(factory);
            });
    }

    createDynamicComponent(html: string) {
        @Component({
            template: html,
        })
        class DynamicComponent {
            constructor(public navCtrl: NavController){}
            deepLink(template,page_to_be_linked) {
                this.navCtrl.push(pageTemplate[template],{
                    pageName: page_to_be_linked
                });
            }
        }
        @NgModule({
            imports: [CommonModule],
            declarations: [DynamicComponent]
        })

        class DynamicComponentModule {}
        return DynamicComponentModule;
    }


}

