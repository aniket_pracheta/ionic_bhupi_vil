import {Injectable} from "@angular/core";
import {AlertController, Events, Platform} from "ionic-angular";
import {HttpClientProvider} from "../../providers/http-client/http-client";
import {Storage} from "@ionic/storage";
import {pageTemplate} from '../../pageTemplate/pageTemplate.json';

declare var FCMPlugin: any;

export interface pushNotificationCount{
    count:any,
    apiStatus:any;

}

@Injectable()
export class PushNotification{
    alert: any;
    deviceToken: any;
    deviceType: any;
    notificationCount: any;
    memberId:any;
    notificationArr: any = [];
    info: pushNotificationCount = { count : 0, apiStatus: '' };
    constructor(public platform: Platform, public alertCtrl: AlertController, public httpClient: HttpClientProvider, private storage: Storage, public events: Events){

        this.getMemberId().then(value => {
            this.getNotificationCount();

        });
        this.getApiStatus().then(data=>{
            this.info.apiStatus = data['status'];
        }).catch(err=>{
            this.info.apiStatus = 401;
        });
    }
    updateUnreadNotificationCnt(){
        //this.info.count = '9';
        this.getNotificationCount();

    }
    public initializePushNotificationProcess(navCtrl): void {
        FCMPlugin.onNotification((data) => {
            if(data.wasTapped){
                //Notification was received on device tray and tapped by the user.
                setTimeout(function(){
                    navCtrl.push(pageTemplate[data.template], {
                        pageName: data.page_to_be_linked
                    });
                },1000);

                //this.showPushNotificationAlert(data.title, data.body, data.notification_id, data.template, data.page_to_be_linked, navCtrl);
            }else{
                //Notification was received in foreground. Maybe the user needs to be notified.
                this.showPushNotificationAlert(data.title, data.body, data.notification_id, data.template, data.page_to_be_linked, navCtrl);

            }
            this.updateUnreadNotificationCnt();
        });
    }
    showPushNotificationAlert(title?:string, msg?:string, id?:string, template?:string, page_to_be_linked?:string, Ref?:any){
        this.alert = this.alertCtrl.create({
            title: title,
            message: msg,
            buttons: [
                {
                    text: 'Ok',
                    role: 'cancel',
                    cssClass: 'alertCancel',
                    handler: () => {
                        if (id != '' || id != undefined){
                            this.updateNotificationStatus(id,this.memberId);
                            if (template == undefined && page_to_be_linked == undefined) {
                                this.alert =null;
                            } else {
                                Ref.push(pageTemplate[template], {
                                    pageName: page_to_be_linked
                                });
                            }
                        } else{
                            this.alert =null;
                        }
                    }
                }
            ]
        });
        this.alert.present();
    }

    getDeviceToken(){
        if (this.platform.is('android') || this.platform.is('ios')) {
            FCMPlugin.getToken(
                (pushRegistrationId: any) => {
                   this.deviceToken = pushRegistrationId;
                },
                (err: any) => {
                    console.log('pushRegistrationId err  '+ err);
                    this.deviceToken = err;
                }
            );
        }
        return this.deviceToken;
    }

    getDeviceType(){
        this.deviceType = this.platform.is('android') ? '0' : '1';
        return this.deviceType;
    }

    getNotificationCount(){
        var data = {
            pageName: 'app-notification-list',
            dataSet: {"member_id": this.memberId}
        }
        return this.httpClient.subCategoryPage(data, 'POST', false).then((data) => {
            console.log('Push Trigeer ' + data['data'][0]['count']);
            return this.info.count = data['data'][0]['count'];
        }).catch((error) => {

        });
        //this.httpClient
    }

    updateNotificationStatus(id, memberId){
        var data = {
            pageName: 'app-notification-status',
            dataSet: {"member_id": memberId, "notification_id":id}
        }
        this.httpClient.subCategoryPage(data, 'POST', false).then((data) => {
            this.updateUnreadNotificationCnt();
        }).catch((error) => {

        });
    }

    getMemberId(){
        return this.storage.get('member_info').then((val) => {
            this.memberId =  val['data'][0]['Patron_Number'];
            return this.memberId;
        }).catch((error) => {
            console.log("Catch Error getMemberId method = ", JSON.stringify(error));
             this.memberId =  '';
        });
        //return this.memberId;
    }

    getApiStatus(){
        return this.storage.get('member_info').then((val) => {
            return this.info.apiStatus =  val;
        }).catch((error) => {
            console.log("Catch Error getMemberId method = ", JSON.stringify(error));
            return this.info.apiStatus =  '';

        });
    }

    getListOfNotification(){
        return this.getMemberId().then(value => {
            var data = {
                pageName: 'app-notification-list',
                dataSet: {"member_id": this.memberId}
            }
            return this.httpClient.subCategoryPage(data, 'POST').then((data) => {
                this.notificationArr = data['data'][0]['notification'];
                if (this.notificationArr === null) {
                    this.notificationArr = [];
                }
                return this.notificationArr;
            }).catch((error) => {
                this.notificationArr = [];
                return this.notificationArr;
            });
        });
    }

    publishAnEvent(eventName, eventData){
        this.events.publish(eventName, eventData, Date.now());
    }

    subscribeAnEvent(eventName?:string){
         return this.events;
    }

    removeUserInfo() {
        this.storage.remove('member_info').then(() => {
            this.publishAnEvent('user:logOut', 'removed');
        });
    }

}