import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {DatePipe} from '@angular/common';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {SplashScreen} from '@ionic-native/splash-screen';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics';
import {StatusBar} from '@ionic-native/status-bar';
import {IonicStorageModule} from '@ionic/storage';

import {SQLite, SQLiteObject} from '@ionic-native/sqlite';
import {FCM} from '@ionic-native/fcm';
import {HTTP} from '@ionic-native/http';
import {HttpModule} from '@angular/http';
import {Network} from '@ionic-native/network';
import {InAppBrowser} from '@ionic-native/in-app-browser';


import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    LatLng,
    CameraPosition,
    MarkerOptions,
} from '@ionic-native/google-maps';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {HomeLaunchPage} from '../pages/home-launch/home-launch';
import {PromotionPage} from '../pages/promotion/promotion';
import {PromotionDetailPage} from '../pages/promotion-detail/promotion-detail';
import {DineAllPage} from '../pages/dine-all/dine-all';
import {DineDetailPage} from '../pages/dine-detail/dine-detail';
import {StayAllPage} from '../pages/stay-all/stay-all';
import {StayRoomsPage} from '../pages/stay-rooms/stay-rooms';
import {StayRoomDetailsPage} from '../pages/stay-room-details/stay-room-details';
import {StayFacilitiesPage} from '../pages/stay-facilities/stay-facilities';
import {StayPackagesPage} from '../pages/stay-packages/stay-packages';
import {StayCorporatePage} from '../pages/stay-corporate/stay-corporate';
import {AboutPage} from '../pages/about/about';
import {ContactPage} from '../pages/contact/contact';
import {EventPage} from '../pages/event/event';
import {SettingPage} from '../pages/setting/setting';
import {ContactFormPage} from '../pages/contact/contact-form/contact-form';
import {LocationPage} from '../pages/contact/location/location';
import {NotificationPage} from '../pages/notification/notification';
import {EventDetailPage} from '../pages/event-detail/event-detail';
import {LoginPage} from '../pages/login/login';
import {JoinPage} from '../pages/join/join';
import {ResponsibleServicePage} from '../pages/responsible-service/responsible-service';
import {MembershipInfoPage} from '../pages/membership-info/membership-info';
import {AccountDetailPage} from '../pages/account-detail/account-detail';


/* Import Providers */
import {HttpClientProvider} from '../providers/http-client/http-client';
import {CustomHeaderComponent} from '../components/custom-header/custom-header';
import {ShareFooterComponent} from '../components/share-footer/share-footer';
import {LogProvider} from '../providers/log/log';
import {GlobalVarsProvider} from '../providers/global-vars/global-vars';
import {LazyImgComponent} from "../global/components/lazy-img/lazy-img.component";
import {LazyLoadDirective} from "../global/directives/lazy-load.directive";
import {ImgcacheService} from "../global/services/cache-img/cache-img.service";
import {MytestPage} from "../pages/mytest/mytest";
import {ContainerRefs} from "../global/components/container-refs";
import {PushNotification} from "../global/components/push-notification";
import {StayBookPage} from "../pages/stay-book/stay-book";
import {FbaProvider} from '../providers/fba/fba';


// import {
//  GoogleMaps,
//  GoogleMap,
//  GoogleMapsEvent,
//  LatLng,
//  CameraPosition,
//  MarkerOptions,

// } from '@ionic-native/google-maps';

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        HomeLaunchPage,
        PromotionPage,
        PromotionDetailPage,
        DineAllPage,
        DineDetailPage,
        StayAllPage,
        StayRoomsPage,
        StayRoomDetailsPage,
        StayFacilitiesPage,
        StayPackagesPage,
        StayCorporatePage,
        AboutPage,
        ContactPage,
        EventPage,
        SettingPage,
        ContactFormPage,
        NotificationPage,
        EventDetailPage,
        LocationPage,
        LoginPage,
        JoinPage,
        ResponsibleServicePage,
        MembershipInfoPage,
        AccountDetailPage,
        CustomHeaderComponent,
        ShareFooterComponent,
        LazyImgComponent,
        LazyLoadDirective,
        MytestPage,
        StayBookPage
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicModule.forRoot(MyApp, {
            backButtonText: '',
            platforms: {
                ios: {
                    pageTransition: "ios-transition"
                },
                android: {
                    pageTransition: "ios-transition"
                },
                windows: {
                    pageTransition: "ios-transition"
                }
            }
        }),
        IonicStorageModule.forRoot({
            name: 'ville',
            driverOrder: ['indexeddb', 'sqlite', 'websql']
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        HomeLaunchPage,
        PromotionPage,
        PromotionDetailPage,
        DineAllPage,
        DineDetailPage,
        StayAllPage,
        StayRoomsPage,
        StayRoomDetailsPage,
        StayFacilitiesPage,
        StayPackagesPage,
        StayCorporatePage,
        AboutPage,
        ContactPage,
        EventPage,
        SettingPage,
        ContactFormPage,
        NotificationPage,
        EventDetailPage,
        LocationPage,
        LoginPage,
        JoinPage,
        ResponsibleServicePage,
        MembershipInfoPage,
        AccountDetailPage,
        CustomHeaderComponent,
        ShareFooterComponent,
        LazyImgComponent,
        MytestPage,
        StayBookPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        SQLite,
        HTTP,
        FCM,
        HttpClientProvider,
        Network,
        GoogleMaps,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        LogProvider,
        GlobalVarsProvider,
        ImgcacheService,
        ContainerRefs,
        PushNotification,
        InAppBrowser,
        DatePipe,
        FirebaseAnalytics,
        FbaProvider
    ]
})
export class AppModule {

}