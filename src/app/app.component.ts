import {Component, ViewChild} from '@angular/core';
import {Platform, Nav, ToastController, AlertController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {Storage} from "@ionic/storage";
import {SplashScreen} from '@ionic-native/splash-screen';
import {FirebaseAnalytics} from '@ionic-native/firebase-analytics';
import {LogProvider} from '../providers/log/log';
import {GlobalVarsProvider} from '../providers/global-vars/global-vars';

import {HomeLaunchPage} from '../pages/home-launch/home-launch';
import {HomePage} from '../pages/home/home';
import {PromotionPage} from '../pages/promotion/promotion';
import {DineAllPage} from '../pages/dine-all/dine-all';
import {StayAllPage} from '../pages/stay-all/stay-all';
import {AboutPage} from '../pages/about/about';
import {ContactPage} from '../pages/contact/contact';
import {EventPage} from '../pages/event/event';
import {SettingPage} from '../pages/setting/setting';
import {NotificationPage} from '../pages/notification/notification';
import {LoginPage} from '../pages/login/login';
import {JoinPage} from '../pages/join/join';
import {MembershipInfoPage} from '../pages/membership-info/membership-info';
import {AccountDetailPage} from '../pages/account-detail/account-detail';
import {StayPackagesPage} from '../pages/stay-packages/stay-packages';
import {ImgcacheService} from "../global/services/cache-img/cache-img.service";
import {PushNotification} from "../global/components/push-notification";

export interface pushNotificationCount{
    count:any,
    apiStatus:any;
}


@Component({
    templateUrl: 'app.html'
})

export class MyApp {
    @ViewChild(Nav) navCtrl: Nav;
    rootPage: any = HomeLaunchPage;
    alert : any;
    info:pushNotificationCount;
    isLoggedinUser:any
    constructor(public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, imgcacheService: ImgcacheService, public toastCtrl: ToastController, public alertCtrl: AlertController, public globalVarsProvider: GlobalVarsProvider, public pushNotification: PushNotification, public storage: Storage, public logProvider: LogProvider) {
        this.info = this.pushNotification.info;
        var eventResult = this.pushNotification.subscribeAnEvent('user:LoggedIn');
        eventResult.subscribe('user:LoggedIn',(obj) => {
            this.isLoggedinUser = obj['status'];
           console.log('Event Result + ', obj);
        });
        this.pushNotification.getApiStatus().then( (res) => {
            this.isLoggedinUser = res['status'];
        }).catch((err)=>{
            this.isLoggedinUser = 401;

        })
        platform.ready().then(() => {
            let lastTimeBackPress = 0;
            let timePeriodToExit  = 2000;
                // get current active page
                platform.registerBackButtonAction(() => {
                    if(this.navCtrl.canGoBack()){
                        this.navCtrl.pop({});
                    }else{
                        if(this.alert){
                            this.alert.dismiss();
                            this.alert =null;
                        }else{
                            //this.showToast();
                            this.platform.exitApp();
                        }
                    }
                },5);


            statusBar.styleDefault();
            splashScreen.hide();

            // initialize imgCache library and set root
            imgcacheService.initImgCache().then(() => {
                this.navCtrl.setRoot(this.rootPage);
            });

            //If developer wants to work on web for some process and test and don't use this method
            if (this.globalVarsProvider.isDevice){
                this.pushNotification.initializePushNotificationProcess(this.navCtrl);
                this.pushNotification.getDeviceToken();
                this.pushNotification.getDeviceType();
            }
        });


    }

    showToast() {
        let toast = this.toastCtrl.create({
            message: 'Press Again to exit',
            duration: 1000,
            position: 'bottom'
        });

        toast.onDidDismiss(() => {
            //console.log('Dismissed toast');
            this.platform.exitApp();

        });

        toast.present();
    }
    showAlert() {
        this.alert = this.alertCtrl.create({
            title: 'Exit?',
            message: 'Do you want to exit the app?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'alertCancel',
                    handler: () => {
                        this.alert =null;
                    }
                },
                {
                    text: 'Exit',
                    cssClass: 'alertExit',
                    handler: () => {
                        this.platform.exitApp();
                    }
                }
            ]
        });
        this.alert.present();
    }

    openHome(params) {
        if (!params) params = {};
        this.navCtrl.setRoot(HomePage);
    }

    openPromotion(params) {
        this.navCtrl.push(PromotionPage, {
            pageName: 'app-casino-promotion'
        });

    }

    openDineAll() {
        this.navCtrl.push(DineAllPage, {
            pageName: 'app-block-page?slug=dine' // link page name
        });
    }

    openStayAll() {
        this.navCtrl.push(StayAllPage, {
            pageName: 'app-block-page?slug=stay' // link page name
        });
    }

    openEvent() {
        this.navCtrl.push(StayPackagesPage, {
            pageName: "app-event"
        });
    }

    openAbout() {
        this.navCtrl.push(AboutPage,{
            pageName: 'app-aboutus'

        });
    }

    openSetting() {
        this.navCtrl.push(SettingPage);
    }

    openContact() {
        this.navCtrl.push(ContactPage);
    }

    openNotification() {
        this.navCtrl.push(NotificationPage);
    }

    openLogin() {
        this.navCtrl.push(LoginPage);
    }

    openJoin() {
        this.navCtrl.push(JoinPage);
    }

    openMemberShip() {
        this.navCtrl.push(MembershipInfoPage);
    }

    openAccountDetail() {
        this.storage.get('member_info').then((val) => {
            if (val != null){
                this.navCtrl.push(AccountDetailPage);
            } else {
                this.navCtrl.push(LoginPage);
            }
        }).catch((error) => {
            console.log("Catch Error login = ", JSON.stringify(error));
        });

    }
    logoutFromApp(){
        this.pushNotification.removeUserInfo();
        this.pushNotification.subscribeAnEvent('user:logOut').subscribe('user:logOut', (obj) => {
            this.isLoggedinUser = obj;
        })
    }
}

