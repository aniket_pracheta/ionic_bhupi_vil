# theVille App

https://invis.io/Z2BPAE2M4

### With the Ionic CLI:

```bash
$ sudo npm install -g ionic cordova

```

Then, to run it, cd into `theVille` and run:

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios
```

Substitute ios for android if not on a Mac.
